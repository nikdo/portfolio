import React from 'react'

export default ({ name, desc, showDesc, toggleDesc, level, maxLevel, setLevel }) => (
	<tbody>
		<tr>
			<th onClick={toggleDesc}>
				<span>{name}</span>
			</th>
			<td className="levels">
				{[0, 1, 2, 3, 4, 5].map((l) => (
					<span
						key={l}
						className={`level ${l <= level ? 'selected' : ''} ${l > maxLevel ? 'disabled' : ''}`}
						onClick={() => setLevel(l)}>
					</span>
				))}
			</td>
		</tr>
		{showDesc ? (
			<tr className="desc">
				<td colSpan="2">{desc}</td>
			</tr>
		) : null}
	</tbody>
)
