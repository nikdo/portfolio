import React from 'react'
import _ from 'underscore'

export default ({ characters, selected, onSelect }) => (
	<select id="characters" value={selected} onChange={(e) => onSelect(e.target.value)} >
		{_.map(characters, (character, id) =>
			<option key={id} value={id}>
				{character.name}
			</option>
		)}
	</select>
)
