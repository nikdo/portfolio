import React from 'react'
import Skill from './Skill'

const Skills = ({ skills, displayedDesc, toggleDesc, availablePoints, setLevel }) =>
	<div>
		<p>
			Available skill points: <strong>{availablePoints}</strong>
		</p>
		<table>
			{skills.map((skill) =>
				<Skill
					key={skill.key}
					name={skill.name}
					desc={skill.description}
					showDesc={displayedDesc == skill.key}
					toggleDesc={() => toggleDesc(skill.key)}
					setLevel={(level) => setLevel(skill.key, level)}
					level={skill.level}
					maxLevel={skill.maxLevel} />
			)}
		</table>
	</div>

Skills.propTypes = {
	availablePoints: React.PropTypes.number.isRequired,
	skills: React.PropTypes.arrayOf(React.PropTypes.shape({
		key: React.PropTypes.string.isRequired,
		name: React.PropTypes.string.isRequired,
		description: React.PropTypes.string.isRequired,
		level: React.PropTypes.number.isRequired,
		maxLevel: React.PropTypes.number.isRequired
	})).isRequired,
	setLevel: React.PropTypes.func.isRequired
}

export default Skills
