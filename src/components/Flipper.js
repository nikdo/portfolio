import React from 'react'

export default ({ imageUrl, index, count, spacing, flip, className }) => (
	<div className={'flipper '+ className}>
		<a href="javascript:void(0)"
			className={(index == 0) ? 'prev disabled' : 'prev'}
			onClick={() => flip(-1)}>
				Previous
		</a>
		<div>
			<img src={imageUrl} style={{marginLeft: index * -spacing + 'px'}}/>
		</div>
		<a href="javascript:void(0)"
			className={(index == count - 1) ? 'next disabled' : 'next'}
			onClick={() => flip(+1)}>
				Next
		</a>
	</div>
)
