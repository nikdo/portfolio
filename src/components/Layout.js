import React from 'react'
import Characters from './Characters'
import Flipper from './Flipper'
import Skills from './Skills'
import marked from 'marked'

// open markdown links in a new tab
const customRenderer = new marked.Renderer();
customRenderer.link = (href, title, text) => {
	return `<a target="_blank" href="${href}">${text}</a>`;
}

export default ({ characters, selectedCharacter, onCharacterSelect, head, headCount, flipHead, body, bodyCount, flipBody, availablePoints, skills, displayedDesc, toggleDesc, setLevel }) => (
	<div>

		<section className="intro">
			<h1>Radek Matěj</h1>
		</section>

		<section className="characters">
			<h2>Characters</h2>
			<Characters
				characters={characters}
				selected={selectedCharacter.id}
				onSelect={onCharacterSelect}/>
			<div dangerouslySetInnerHTML={{__html: marked(selectedCharacter.description, { renderer: customRenderer })}}></div>
		</section>

		<section className="appearance">
			<h2>Appearance</h2>
			<Flipper
				imageUrl="img/heads.png"
				index={head}
				count={headCount}
				spacing="125"
				flip={flipHead}
				className="head" />
			<Flipper
				imageUrl="img/bodies.png"
				index={body}
				count={headCount}
				spacing="125"
				flip={flipBody}
				className="body" />
		</section>

		<section className="skills">
			<h2>Skills</h2>
			<Skills
				availablePoints={availablePoints}
				skills={skills}
				displayedDesc={displayedDesc}
				toggleDesc={toggleDesc}
				setLevel={setLevel} />
		</section>

		<section className="links">
			<h2>Learn More</h2>
			<ul className="links">
				<li><a href="http://cz.linkedin.com/pub/radek-matěj/a/132/b14" className="linkedin" title="LinkedIn">LinkedIn</a></li>
				<li><a href="http://twitter.com/nikdo" className="twitter" title="Twitter">Twitter</a></li>
				<li><a href="http://www.facebook.com/nikdo" className="facebook" title="Facebook">Facebook</a></li>
				<li><a href="http://medium.com/@nikdo" className="medium" title="Medium">Medium</a></li>
			</ul>
		</section>

		<section className="hire">
			<p>
				&quot;Starting an exciting adventure with a noble goal?
				I’m eager to provide my skills for something significant.&quot;
			</p>

			<a href="mailto:radek@nikdo.cz" className="hire">Add to Your Team</a>

			<p>* No job offers, just contracts.</p>
		</section>

	</div>
)
