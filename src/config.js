export default {

  totalPoints: 22,

  skills: {
    pm: {
      name: 'Product Management',
      description: 'market research, agile product management, user stories, backlog, lean methodology, product management role in agile organizations, scaling agile development',
      max: 5
    },
    ux: {
      name: 'User Experience',
      description: 'customer experience, user interviews, lean personas, user stories, guerrila usability testing',
      max: 4
    },
    id: {
      name: 'Interaction Design',
      description: 'information architecture, visual hierarchy, prototyping, design workshop facilitation',
      max: 3
    },
    coding: {
      name: 'HTML & CSS',
      description: 'HTML5, semantic markup, CSS3, Sass, CSS modules, cssnext, responsive design, vertical rythm',
      max: 4
    },
    js: {
      name: 'JavaScript',
      description: 'ES6, functional programming, test-driven development, React, Redux, webpack, TypeScript',
      max: 4
    },
    backend: {
      name: 'Backend Development',
      description: 'Node.js, Express, MongoDb, SQL, Jekyll, PHP, WordPress',
      max: 2
    },
    design: {
      name: 'Design & Illustration',
      description: 'hand-drawn doodles, pixel art, minimalistic design, basic vector art and photo editing, typography',
      max: 2
    },
    communication: {
      name: 'Communication',
      max: 5,
      description: 'democratic leadership, problem articulation, presentation, meeting facilitation'
    },
    teaching: {
      name: 'Teaching',
      description: 'mentoring in all areas of expertise, presentations, workshops, conferrence talks',
      max: 5
    }
  },

  characters: {
    developer: {
      name: 'Developer',
      description: 'Transforms ideas into a working product. Keeps it simple. Resistant to a premature optimalization.',
      skills: {
        pm: 1,
        communication: 3,
        teaching: 1,
        ux: 1,
        id: 2,
        coding: 4,
        js: 4,
        backend: 2,
        design: 0
      },
      head: 9,
      body: 4
    },
    designer: {
      name: 'UX Designer',
      description: 'Understands users. Creates interfaces that make them happy. Intolerant to products causing frustration.',
      skills: {
        pm: 2,
        communication: 4,
        teaching: 2,
        ux: 4,
        id: 3,
        coding: 1,
        js: 1,
        backend: 0,
        design: 1
      },
      head: 8,
      body: 6
    },
    po: {
      name: 'Product Owner',
      description: 'Leads the team to discover the product. Product that solves a real pain. Immune to unfounded dreams.',
      skills: {
        pm: 5,
        communication: 5,
        teaching: 3,
        ux: 2,
        id: 1,
        coding: 0,
        js: 0,
        backend: 0,
        design: 0
      },
      head: 2,
      body: 8
    },
    artist: {
      name: 'Artist',
      description: 'Plays with pixels. Draws doodles. Worships whitespace. Believes that good design lacks decoration.',
      skills: {
        pm: 0,
        communication: 2,
        teaching: 0,
        ux: 1,
        id: 2,
        coding: 2,
        js: 1,
        backend: 0,
        design: 2
      },
      head: 0,
      body: 0
    },
    mentor: {
      name: 'Mentor',
      description: `Handovers his skills to others, provides guidance, discovers potential. Respectful to comfort zone.

[Naučmese.cz Product Management training](https://www.naucmese.cz/kurz/product-management-i-cesta-muze-byt-cil)`,
      skills: {
        pm: 3,
        communication: 5,
        teaching: 5,
        ux: 2,
        id: 1,
        coding: 1,
        js: 1,
        backend: 0,
        design: 0
      },
      head: 11,
      body: 11
    },
    polymath: {
      name: 'Polymath',
      description: 'Employs all his diverse skills to deliver what\'s needed. Sceptical about value of specialization.',
      skills: {
        pm: 2,
        communication: 5,
        teaching: 3,
        ux: 2,
        id: 2,
        coding: 2,
        js: 2,
        backend: 2,
        design: 2
      },
      head: 6,
      body: 3
    },
    custom: {
      name: 'Custom',
      description: 'Who do you need? Which skills do you require?',
      skills: {
        pm: 0,
        communication: 0,
        teaching: 0,
        ux: 0,
        id: 0,
        coding: 0,
        js: 0,
        backend: 0,
        design: 0
      },
      head: 3,
      body: 9
    }
  }
}
