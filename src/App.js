import React, { Component } from 'react'
import _ from 'underscore'
import config from './config'
import Layout from './components/Layout'

const headCount = 12

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = this.getCharacterState('developer')

    this.setLevel = this.setLevel.bind(this)
    this.getCharacterState = this.getCharacterState.bind(this)
  }

  setLevel (key, level) {
    level = Math.min(level, config.skills[key].max)

    const { availablePoints } = this.state
    const prevLevel = this.state.skills[key]
    const possibleDifference = Math.min(availablePoints, level - prevLevel)

    this.setState({
      skills: { ...this.state.skills, [`${key}`]: prevLevel + possibleDifference },
      availablePoints: availablePoints - possibleDifference
    })
  }

  getCharacterState (id) {
    const character = config.characters[id]
    return {
      skills: character.skills,
      head: character.head,
      body: character.body,
      character: {
        ...character,
        id: id
      },
      availablePoints: config.totalPoints - _.reduce(character.skills, (sum, points) => sum + points)
    }
  }

  render () {
    return <Layout
      characters={config.characters}
      selectedCharacter={this.state.character}
      onCharacterSelect={(id) => this.setState(this.getCharacterState(id))}
      head={this.state.head}
      headCount={headCount}
      flipHead={(inc) => this.setState({head: Math.max(0, Math.min(headCount - 1, (this.state.head + inc)))})}
      body={this.state.body}
      bodyCount={headCount}
      flipBody={(inc) => this.setState({body: Math.max(0, Math.min(headCount - 1, (this.state.body + inc)))})}
      availablePoints={this.state.availablePoints}
      skills={_.map(config.skills, (skill, key) => ({
        key: key,
        name: skill.name,
        description: skill.description,
        level: this.state.skills[key],
        maxLevel: skill.max
      }))}
      displayedDesc={this.state.description}
      toggleDesc={(key) => this.setState({description: this.state.description === key ? null : key})}
      setLevel={(key, level) => this.setLevel(key, level)} />
  }
}
