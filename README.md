# Portfolio

My personal portfolio hosted on http://nikdo.cz.


# Build & Run

1. `npm install`
2. `npm start`
3. Open `dist/index.html` in browser.


# Deploy

1. `npm install`
2. `npm run deploy`
